from setuptools import setup, find_packages

setup(
    name="auto_updater",
    version="0.1-dev",
    scripts=["./auto_updater"],
    extras_require={
        "test": ["pytest>=5.4.2", "mock>=3.0.5"],
    },
    install_requires=[
        "python-gitlab>=1.12.1",
        "GitPython>=3.0.4",
        "tabulate>=0.8.7",
    ],
    packages=find_packages(),
)
