import pytest
import mock
from src.element import BstElement


@pytest.mark.parametrize(
    "element",
    [
        pytest.param("extensions/mesa-git/mesa.bst"),
        pytest.param("include/ffmpeg.yml"),
        pytest.param("components/cython.bst"),
        pytest.param("components/xorg-proto-xcb.bst"),
        pytest.param("components/python3-markdown.bst"),
    ],
)
def test_element_creation_name(element):
    assert BstElement(element).element == element


@pytest.mark.parametrize(
    "element,oldVersion,newVersion",
    [
        ("extensions/mesa-git/mesa.bst", "", ""),
        ("include/ffmpeg.yml", "", "1.0.0"),
        ("components/cython.bst", "1.0.0", "2.0.0"),
    ],
)
def test_element_dict_creation(element, oldVersion, newVersion):
    assert BstElement(element, oldVersion, newVersion).asDict() == {
        "Element": element,
        "Old Version": oldVersion,
        "New Version": newVersion,
        "MR Created": False,
    }


@pytest.mark.parametrize(
    "mrCreated",
    [True, False],
)
def test_element_mr_creation(mrCreated):
    e = BstElement("opencv.bst", "1.0.0", "2.0.0")
    e.mrCreated = mrCreated
    assert e.asDict() == {
        "Element": "opencv.bst",
        "Old Version": "1.0.0",
        "New Version": "2.0.0",
        "MR Created": mrCreated,
    }
