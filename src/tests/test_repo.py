import pytest
import mock
from src.repo import BstRepo


@mock.patch("src.repo.git.Repo")
def test_create_branches_git_tag(mock_repo):
    mock_repo().active_branch = "master"
    mock_repo().git.status.return_value = """
        On branch master
            Changes not staged for commit:
                (use "git add <file>..." to update what will be committed)
                (use "git checkout -- <file>..." to discard changes in working directory)

	                modified:   elements/opencv.bst
        """
    mock_repo().git.diff.return_value = """
        diff --git a/elements/opencv.bst b/elements/opencv.bst
        index 0d3ed48..71b0890 100644
        --- a/elements/opencv.bst
        +++ b/elements/opencv.bst
        @@ -62,9 +62,9 @@ sources:
        - kind: git_tag
        url: https://github.com/opencv/opencv.git
        track: master
        -  ref: 4.2.0-0-gbda89a6469aa79ecd8713967916bd754bff1d931
        +  ref: 4.3.0-0-g01b2c5a77ca6dbef3baef24ebc0a5984579231d9
        - kind: git_tag
        directory: contrib
        url: https://github.com/opencv/opencv_contrib.git
        track: master
        -  ref: 4.2.0-0-g65abc7090dedc84bbedec4dfd143f0340e52114f
        +  ref: 4.3.0-0-ge6f32c6a69043456a806a4e802ee3ce7b7059c93
        -- 
    """
    with BstRepo() as repo:
        e = repo.bst_element_branches[0]
        assert e[0] == "update/opencv.bst-diff_md5-1f37516-for-master"
        assert e[1].element == "opencv.bst"
        assert (
            e[2].strip()
            == """
Update elements/opencv.bst to 4.3.0-0

Updates elements/opencv.bst to 4.3.0-0 (01b2c5a)
Previous version was 4.2.0-0 (bda89a6)
            """.strip()
        )


@mock.patch("src.repo.git.Repo")
def test_create_branches_git_tag_multiple_elements(mock_repo):
    mock_repo().active_branch = "master"
    mock_repo().git.status.return_value = """
        On branch master
            Changes not staged for commit:
                (use "git add <file>..." to update what will be committed)
                (use "git checkout -- <file>..." to discard changes in working directory)

	                modified:   elements/extensions/mesa-git/mesa.bst
                    modified:   elements/extensions/mesa/mesa.bst
        """
    mock_repo().git.diff.side_effect = [
        """
        diff --git a/elements/extensions/mesa-git/mesa.bst b/elements/extensions/mesa-git/mesa.bst
        index 0d3ed48..71b0890 100644
        --- a/elements/extensions/mesa-git/mesa.bst
        +++ b/elements/extensions/mesa-git/mesa.bst
        @@ -62,9 +62,9 @@ sources:
        - kind: git_tag
        url: freedesktop:mesa/mesa.git
        track: master
        -  ref: 19.3-branchpoint-2509-g951083768b351b0700bdcc02758670e505cce974
        +  ref: 20.1-branchpoint-749-gc4544f47167ab5fe170e5131ad45b20b02507bce
    """,
        """
        diff --git a/elements/extensions/mesa/mesa.bst b/elements/extensions/mesa/mesa.bst
        index 0d3ed48..71b0890 100644
        --- a/elements/extensions/mesa/mesa.bst
        +++ b/elements/extensions/mesa/mesa.bst
        @@ -62,9 +62,9 @@ sources:
        - kind: git_tag
        url: freedesktop:mesa/mesa.git
        track: master
        -  ref: 19.3-branchpoint-2509-g951083768b351b0700bdcc02758670e505cce974
        +  ref: 20.1-branchpoint-749-gc4544f47167ab5fe170e5131ad45b20b02507bce
    """,
    ]

    with BstRepo() as repo:
        e = repo.bst_element_branches[0]
        assert (
            e[0]
            == "update/extensions_mesa-git_mesa.bst-diff_md5-3dfe19e-for-master"
        )
        assert e[1].element == "extensions/mesa-git/mesa.bst"
        assert (
            e[2].strip()
            == """
Update elements/extensions/mesa-git/mesa.bst to 20.1-branchpoint-749

Updates elements/extensions/mesa-git/mesa.bst to 20.1-branchpoint-749 (c4544f4)
Previous version was 19.3-branchpoint-2509 (9510837)
            """.strip()
        )
        e = repo.bst_element_branches[1]
        assert (
            e[0]
            == "update/extensions_mesa_mesa.bst-diff_md5-cb8f0e0-for-master"
        )
        assert e[1].element == "extensions/mesa/mesa.bst"
        assert (
            e[2].strip()
            == """
Update elements/extensions/mesa/mesa.bst to 20.1-branchpoint-749

Updates elements/extensions/mesa/mesa.bst to 20.1-branchpoint-749 (c4544f4)
Previous version was 19.3-branchpoint-2509 (9510837)
            """.strip()
        )


@mock.patch("src.repo.git.Repo")
def test_create_branches_git_tag_commits_off_tag(mock_repo):
    mock_repo().active_branch = "master"
    mock_repo().git.status.return_value = """
        On branch master
            Changes not staged for commit:
                (use "git add <file>..." to update what will be committed)
                (use "git checkout -- <file>..." to discard changes in working directory)

	                modified:   elements/opencv.bst
        """
    mock_repo().git.diff.return_value = """
        diff --git a/elements/opencv.bst b/elements/opencv.bst
        index 0d3ed48..71b0890 100644
        --- a/elements/opencv.bst
        +++ b/elements/opencv.bst
        @@ -62,9 +62,9 @@ sources:
        - kind: git_tag
        url: https://github.com/opencv/opencv.git
        track: master
        -  ref: 4.2.0-112-gbda89a6469aa79ecd8713967916bd754bff1d931
        +  ref: 4.3.0-64-g01b2c5a77ca6dbef3baef24ebc0a5984579231d9
        - kind: git_tag
        directory: contrib
        url: https://github.com/opencv/opencv_contrib.git
        track: master
        -  ref: 4.2.0-112-g65abc7090dedc84bbedec4dfd143f0340e52114f
        +  ref: 4.3.0-64-ge6f32c6a69043456a806a4e802ee3ce7b7059c93
        -- 
    """

    with BstRepo() as repo:
        e = repo.bst_element_branches[0]
        assert e[0] == "update/opencv.bst-diff_md5-25f98d6-for-master"
        assert e[1].element == "opencv.bst"
        assert (
            e[2].strip()
            == """
Update elements/opencv.bst to 4.3.0-64

Updates elements/opencv.bst to 4.3.0-64 (01b2c5a)
Previous version was 4.2.0-112 (bda89a6)
            """.strip()
        )


@mock.patch("src.repo.git.Repo")
def test_create_branches_git_sha(mock_repo):
    mock_repo().active_branch = "19.08"
    mock_repo().git.status.return_value = """
        On branch master
            Changes not staged for commit:
                (use "git add <file>..." to update what will be committed)
                (use "git checkout -- <file>..." to discard changes in working directory)

	                modified:   elements/bootstrap/gnu-config.bst
        """
    mock_repo().git.diff.return_value = """
        diff --git a/elements/bootstrap/gnu-config.bst b/elements/bootstrap/gnu-config.bst
        index 3546067e..43660a2a 100644
        --- a/elements/bootstrap/gnu-config.bst
        +++ b/elements/bootstrap/gnu-config.bst
        @@ -9,7 +9,7 @@ sources:
        - kind: git_tag
        url: savannah:config.git
        track: master
        -  ref: 04b07fc6d9ad4fd2be7434c9ce8b03341e54ca02
        +  ref: 5256817ace8493502ec88501a19e4051c2e220b0
    """

    with BstRepo() as repo:
        e = repo.bst_element_branches[0]
        assert (
            e[0]
            == "update/bootstrap_gnu-config.bst-diff_md5-15e205b-for-19.08"
        )
        assert e[1].element == "bootstrap/gnu-config.bst"
        assert (
            e[2].strip()
            == """
Update elements/bootstrap/gnu-config.bst to 5256817

Updates elements/bootstrap/gnu-config.bst to 5256817
Previous version was 04b07fc
            """.strip()
        )


@mock.patch("src.repo.git.Repo")
def test_create_branches_pypi(mock_repo):
    mock_repo().active_branch = "22.08"
    mock_repo().git.status.return_value = """
        On branch master
            Changes not staged for commit:
                (use "git add <file>..." to update what will be committed)
                (use "git checkout -- <file>..." to discard changes in working directory)

	                modified:   elements/components/python3-jinja2.bst
        """
    mock_repo().git.diff.return_value = """
    diff --git a/elements/components/python3-jinja2.bst b/elements/components/python3-jinja2.bst
    index d2affc6ca..7544c09ae 100644
    --- a/elements/components/python3-jinja2.bst
    +++ b/elements/components/python3-jinja2.bst
    @@ -13,5 +13,5 @@ sources:
    - kind: pypi
    name: Jinja2
    ref:
    -    sha256sum: 640bed4bb501cbd17194b3cace1dc2126f5b619cf068a726b98192a0fde74ae9
    -    suffix: 89/e3/b36266381ae7a1310a653bb85f4f3658c462a69634fa9b2fef76252a50ed/Jinja2-3.1.1.tar.gz
    +    sha256sum: 31351a702a408a9e7595a8fc6150fc3f43bb6bf7e319770cbc0db9df9437e852
    +    suffix: 7a/ff/75c28576a1d900e87eb6335b063fab47a8ef3c8b4d88524c4bf78f670cce/Jinja2-3.1.2.tar.gz
    """

    with BstRepo() as repo:
        e = repo.bst_element_branches[0]
        assert (
            e[0]
            == "update/components_python3-jinja2.bst-diff_md5-fda9633-for-22.08"
        )
        assert e[1].element == "components/python3-jinja2.bst"
        assert (
            e[2].strip()
            == """
Update elements/components/python3-jinja2.bst to 3.1.2

Updates elements/components/python3-jinja2.bst to 3.1.2
Previous version was 3.1.1
            """.strip()
        )
