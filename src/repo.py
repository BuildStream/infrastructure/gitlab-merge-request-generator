import git
import gitlab
import hashlib
import os
from pathlib import Path
import re
from src.element import BstElement
from typing import NamedTuple
import textwrap


class ParsingException(Exception):
    pass


class Branch(NamedTuple):
    name: str
    element: BstElement
    commitMsg: str


class BstRepo:
    def __init__(
        self,
        branch=None,
        ignore_elements=None,
        overwrite=False,
        verbose=False,
        project_root=Path("."),
    ):
        self.repo = git.Repo(".")
        self.project_root = project_root
        active_branch = ""
        try:
            active_branch = self.repo.active_branch
        except:
            # We're in a detached HEAD, instead store the current sha
            active_branch = self.repo.head.object.hexsha
        if branch:
            self.base_branch = branch
        else:
            self.base_branch = active_branch

        self.original_branch = active_branch
        self.elements = self._get_updated_elements()
        if ignore_elements:
            self.elements = list(set(self.elements) - set(ignore_elements))
        self.bst_element_branches = []
        self.overwrite = overwrite

    def __enter__(self):
        self.create_branches()
        return self

    def __exit__(self, *args):
        self.repo.head.reset(index=True, working_tree=True)
        self.reset()

    def _get_updated_elements(self):
        return re.findall(r"elements\/.*", self.repo.git.status())

    def _list_remote_branches(self):
        return [
            re.sub("^origin/", "", refs.name)
            for refs in self.repo.remote().refs
        ]

    def _get_commit_data(self, element, diff):
        GIT_TAG_FORMAT = r"\s+ref:\s+(.+)-(\d+)-g(.+)"
        SHA_FORMAT = r"\s+ref:\s+(.+)"
        PYTHON_FORMAT = r".+/[^/]+-(?P<version>\d[^/]*)[.](?:tgz|tar[.].*|zip)"
        diff_md5 = hashlib.md5(diff.encode("utf-8")).hexdigest()[:7]
        if re.search(r"\+" + GIT_TAG_FORMAT, diff):
            new = re.search(r"\+" + GIT_TAG_FORMAT, diff)
            new_version_slug = new.group(1)
            new_tag_offset = new.group(2)
            new_sha = new.group(3)[:7]

            old = re.search(r"\-" + GIT_TAG_FORMAT, diff)
            prev_version_message = ""
            old_version_slug = ""
            if old:
                old_version_slug = old.group(1)
                old_tag_offset = old.group(2)
                old_sha = old.group(3)[:7]
                prev_version_message = (
                    "Previous version was {0}-{1} ({2})".format(
                        old_version_slug, old_tag_offset, old_sha
                    )
                )
            else:
                prev_version_message = "Previous version could not be parsed"

            commit_message = (
                "Update {0} to {1}-{2}\n\n".format(
                    element, new_version_slug, new_tag_offset
                )
                + "Updates {0} to {1}-{2} ({3})\n".format(
                    element, new_version_slug, new_tag_offset, new_sha
                )
                + prev_version_message
            )
            return commit_message, new_version_slug, old_version_slug, diff_md5
        elif re.search(r"\+" + SHA_FORMAT, diff):
            new = re.search(r"\+" + SHA_FORMAT, diff)
            new_sha = new.group(1)[:7]

            old = re.search(r"\-" + SHA_FORMAT, diff)
            old_sha = ""
            prev_version_message = ""
            if old:
                old_sha = old.group(1)[:7]
                prev_version_message = "Previous version was {0}".format(
                    old_sha
                )
            else:
                prev_version_message = "Previous version could not be parsed"

            commit_message = (
                "Update {0} to {1}\n\n".format(element, new_sha)
                + "Updates {0} to {1}\n".format(element, new_sha)
                + prev_version_message
            )
            return commit_message, new_sha, old_sha, diff_md5
        elif re.search(r"[+]" + PYTHON_FORMAT, diff):
            new = re.search(r"[+]" + PYTHON_FORMAT, diff)
            new_version = new.group("version")
            old_version = ""
            old = re.search(r"[-]" + PYTHON_FORMAT, diff)
            if old:
                old_version = old.group("version")
                previous_message = f"Previous version was {old_version}"
            else:
                previous_message = f"Previous version could not be parsed"
            commit_message = textwrap.dedent(
                f"""\
                Update {element} to {new_version}

                Updates {element} to {new_version}
                {previous_message}
                """
            )
            return commit_message, new_version, old_version, diff_md5

        else:
            raise ParsingException(
                f"Failed to parse version from {element} diff {diff} "
            )

    def create_branches(self):
        repo = self.repo
        for e in self.elements:
            diff = repo.git.diff(self.base_branch, self.project_root / e)
            msg, new_sha, old_sha, diff_md5 = self._get_commit_data(
                element=e, diff=diff
            )
            branch_name = (
                "update/"
                + e.replace("elements/", "").replace("/", "_")
                + "-diff_md5-"
                + diff_md5
                + "-for-"
                + self.base_branch
            )
            try:
                branch = repo.create_head(branch_name, self.base_branch)
            except OSError:
                if self.overwrite:
                    print("deleting branch {0}".format(branch_name))
                    repo.delete_head(branch_name, "-D")
                    branch = repo.create_head(branch_name, self.base_branch)
                else:
                    print(
                        "Update branch {0} already created".format(branch_name)
                    )
                    continue
            self.checkout(branch)
            repo.index.add([self.project_root / e])
            repo.index.commit(msg)
            self._reset_to_base_branch()
            element = BstElement(e.replace("elements/", ""), old_sha, new_sha)
            self.bst_element_branches.append(Branch(branch_name, element, msg))

        repo.head.reset(index=True, working_tree=True)
        return self.bst_element_branches

    def checkout(self, branch):
        self.repo.git.checkout(branch)

    # Return to the base branch that all update branches are based on
    # To be used internally by the class
    def _reset_to_base_branch(self):
        self.repo.git.checkout(self.base_branch)

    # Return to the branch before the updater was invoked
    def reset(self):
        self.repo.git.checkout(self.original_branch)

    def _gitlab_project(self, gl, gitlab_project):
        return gl.projects.get(gitlab_project, lazy=True)

    def _open_mr_exists(self, gl, gitlab_project, src_branch, target_branch):
        mr_list = self._gitlab_project(gl, gitlab_project).mergerequests.list(
            state="opened",
            source_branch=src_branch,
            target_branch=target_branch,
        )
        return bool(mr_list)

    def _push_to_remote(
        self,
        branch,
        remote="origin",
        force=False,
    ):
        success = True
        try:
            self.checkout(branch)
            if force:
                self.repo.git.push("-f", "--set-upstream", remote, branch)
                print("Force pushed branch {0} to remote".format(branch))
            elif not force and branch in self._list_remote_branches():
                print("Skipped pushing branch {0} to remote".format(branch))
            else:
                self.repo.git.push("--set-upstream", remote, branch)
                print("Pushed branch {0} to remote".format(branch))
        except git.exc.GitCommandError as e:
            success = False
            print("Failed to push branch {0} to remote: {1}".format(branch, e))

        return success

    def create_merge_request(
        self,
        gl,
        branch,
        element,
        gitlab_project,
        gitlab_url="https://gitlab.com/",
    ):

        project = self._gitlab_project(gl, gitlab_project)
        project.mergerequests.create(
            {
                "source_branch": branch,
                "target_branch": self.base_branch,
                "title": "Update {0}".format(element.element),
                "remove_source_branch": "true",
            }
        )
        print("created MR for branch {0}".format(branch))
