from pathlib import Path
import subprocess


class BstElement:
    def __init__(
        self,
        element,
        oldVersion="",
        newVersion="",
        bst1=True,
        project_root=Path("."),
    ):
        self.element = element
        self.oldVersion = oldVersion
        self.newVersion = newVersion
        self._mrCreated = False
        self._bst1 = bst1
        self._project_root = str(project_root)

    def __repr__(self):
        return "BstElement(element={0})".format(self.element)

    def asDict(self):
        return {
            "Element": self.element,
            "Old Version": self.oldVersion,
            "New Version": self.newVersion,
            "MR Created": self.mrCreated,
        }

    def _bst(self, *args, verbose=False, element=None, on_error="terminate"):
        pipe = None
        if not verbose:
            pipe = subprocess.DEVNULL

        check = True
        if on_error == "continue":
            check = False

        # If the project root differs from the current directory, point
        # Buildstream at it.
        build_args = []
        if self._project_root:
            build_args.append("-C")
            build_args.append(self._project_root)

        element_to_build = self.element
        if element:
            element_to_build = element
        # Terminate process if any input is required
        result = subprocess.run(
            [
                "bst",
                f"--on-error={on_error}",
                *build_args,
                *args,
                element_to_build,
            ],
            check=check,
            stdout=pipe,
            stderr=pipe,
        )
        return result.returncode == 0

    @property
    def mrCreated(self):
        return self._mrCreated

    @mrCreated.setter
    def mrCreated(self, mrCreated):
        self._mrCreated = mrCreated

    def build(self, verbose=False, element=None):
        try:
            self._bst("build", verbose=verbose, element=element)
            print("Built element {0}".format(self.element))
            return True
        except subprocess.CalledProcessError:
            print("Element {0} failed to build".format(self.element))
            return False

    def track(self, deps="all", verbose=False, on_error="quit"):
        if self._bst1:
            return self._bst(
                "track",
                "--deps={0}".format(deps),
                verbose=verbose,
                on_error=on_error,
            )
        else:
            return self._bst(
                "source",
                "track",
                "--deps={0}".format(deps),
                verbose=verbose,
                on_error=on_error,
            )
